import { Module } from '@nestjs/common';
import { mongoUrl } from './config/mongoose.config';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { ItemsModule } from './items/items.module';

@Module({
  imports: [
    MongooseModule.forRoot(mongoUrl, {
      useCreateIndex: true,
      useFindAndModify: false
    }),
    AuthModule,
    ItemsModule
  ],
})
export class AppModule {}
