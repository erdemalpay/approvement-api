import { Module } from '@nestjs/common';
import { ItemsController } from './items.controller';
import { ItemsService } from './items.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Item, ItemSchema } from './item.schema';
import { ItemRepository } from './item.repository';
import { AuthModule } from '../auth/auth.module';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Item.name, schema: ItemSchema }]),
    AuthModule,
    PassportModule.register({
      defaultStrategy: 'jwt'
    }),
  ],
  controllers: [ItemsController],
  providers: [ItemsService, ItemRepository]
})
export class ItemsModule {}
