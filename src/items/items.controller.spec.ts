import { Test, TestingModule } from '@nestjs/testing';
import { ItemsController } from './items.controller';
import { CreateItemsDto } from './dto/create-items.dto';
import { ItemType } from './item-type.enum';
import { ItemsService } from './items.service';
import { UpdateItemDto } from './dto/update-item.dto';

const mockItemService = () => ({
  createItems: jest.fn()
});

describe('Items Controller', () => {
  let controller: ItemsController;
  let itemsService: ItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ItemsController],
      providers: [
        {
          provide: ItemsService,
          useFactory: mockItemService
        }
      ]
    }).compile();

    controller = module.get<ItemsController>(ItemsController);
    itemsService = module.get<ItemsService>(ItemsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  
  describe('createItem', () => {
    it('should save item in provided payload and return item', async () => {
      const testCreateItemsDto : CreateItemsDto = {
        items: [],
        source: 'hurriyet',
        type: ItemType.HEADLINES,
      };
        await controller.createItems(testCreateItemsDto);
        expect(itemsService.createItems).toHaveBeenCalledWith(testCreateItemsDto);
    });
  });

  describe('getSingleItemByType', () => {
    const testItem = { dummy: 'item' };
    beforeEach(() => {
      itemsService.getSingleItemByType = jest.fn().mockReturnValue(testItem);
    });
    it('should call getSingleItemByType method of service', async () => {
      expect(itemsService.getSingleItemByType).not.toHaveBeenCalled();
      const result = await controller.getSingleItemByType(ItemType.HEADLINES);
      expect(itemsService.getSingleItemByType).toHaveBeenCalledWith(ItemType.HEADLINES);
      expect(result).toEqual(testItem);
    });
  });
  
  describe('getApprovedItemsByType', () => {
    const testItem = { dummy: 'item' };
    beforeEach(() => {
      itemsService.getApprovedItemsByType = jest.fn().mockReturnValue(testItem);
    });
    it('should call getApprovedItemsByType method of service', async () => {
      expect(itemsService.getApprovedItemsByType).not.toHaveBeenCalled();
      const result = await controller.getApprovedItemsByType(ItemType.HEADLINES);
      expect(itemsService.getApprovedItemsByType).toHaveBeenCalledWith(ItemType.HEADLINES);
      expect(result).toEqual(testItem);
    });
  });
  
  describe('setApprovalStatus', () => {
    const testItem = { dummy: 'item' };
    const id = 'test_id';
    const status = true;
    const setApprovalStatusDto = {
      id,
      status
    };
    beforeEach(() => {
      itemsService.setApprovalStatus = jest.fn().mockReturnValue(testItem);
    });
    it('should call setApprovalStatus method of service', async () => {
      expect(itemsService.setApprovalStatus).not.toHaveBeenCalled();
      const result = await controller.setApprovalStatus(id, status);
      expect(itemsService.setApprovalStatus).toHaveBeenCalledWith(setApprovalStatusDto);
      expect(result).toEqual(testItem);
    });
  });

  describe('updateItem', () => {
    const testItem = { dummy: 'item' };
    const id = 'test_id';
    const source = 'test_source';
    const updateItemDto: UpdateItemDto = {
      source
    };
    beforeEach(() => {
      itemsService.updateItem = jest.fn().mockReturnValue(testItem);
    });
    it('should call updateItem method of service', async () => {
      expect(itemsService.updateItem).not.toHaveBeenCalled();
      const result = await controller.updateItem(id, updateItemDto);
      expect(itemsService.updateItem).toHaveBeenCalledWith(id, updateItemDto);
      expect(result).toEqual(testItem);
    });
  });
});
