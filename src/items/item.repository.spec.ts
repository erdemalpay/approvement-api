import { Test } from '@nestjs/testing';
import { ItemRepository } from './item.repository';
import { getModelToken } from '@nestjs/mongoose';
import { Item } from './item.schema';
import { ItemType } from './item-type.enum';
import { ConflictException, NotFoundException } from '@nestjs/common';
import { Error } from 'mongoose';

const mockItemModel = () => ({});
const testItem = {
  id: 'test-item',
  data: { test: 'data' },
  type: ItemType.HEADLINES,
  source: 'test_source'
};
const testItem2 = {
  id: 'test-item',
  data: { test: 'data' },
  type: ItemType.HEADLINES,
  source: 'test_source',
  approved: true
};
const testItems = [testItem, testItem2];

describe('Items Repository', () => {
  let itemRepository: ItemRepository;
  let itemModel;

  beforeEach(async () => {
    const itemModelToken = getModelToken(Item.name);
    const module = await Test.createTestingModule({
      providers: [
        ItemRepository, {
          provide: itemModelToken,
          useFactory: mockItemModel,
       }
      ]
    }).compile();
    itemRepository = await module.get<ItemRepository>(ItemRepository);
    itemModel = await module.get<Item>(itemModelToken);
  });

  it('should be defined', () => {
    expect(itemRepository).toBeDefined();
  });

  describe('create', () => {
    let toObject;

    it('should save an item an return it', async () => {
      toObject = jest.fn().mockReturnValue(testItem);
      itemModel.create = jest.fn().mockResolvedValue({ toObject });
      expect(itemModel.create).not.toHaveBeenCalled();
      const createResult = await itemRepository.create(testItem);
      expect(itemModel.create).toHaveBeenCalled();
      expect(createResult).toEqual(testItem);
    });
    
    it('should return already exists error', async () => {
      itemModel.create = jest.fn().mockRejectedValue({ code: 11000 });
      const promise = itemRepository.create(testItem);
      await expect(promise).rejects.toThrow(ConflictException);
    });
    
    it('should throw incoming error', async () => {
      itemModel.create = jest.fn().mockRejectedValue(new Error('12345'));
      const promise = itemRepository.create(testItem);
      await expect(promise).rejects.toThrow(Error);
    });
  });
  
  describe('getSingleItemByType', () => {
    let toObject;
    it('should return one unapproved item by provided type', async () => {
      toObject = jest.fn().mockReturnValue(testItem);
      itemModel.findOne = jest.fn().mockResolvedValue( { toObject });
      expect(itemModel.findOne).not.toHaveBeenCalled();
      const result = await itemRepository.getSingleItemByType(ItemType.HEADLINES);
      expect(itemModel.findOne).toHaveBeenCalledWith({ type: ItemType.HEADLINES, approved: { $exists: false }});
      expect(result).toEqual(testItem);
    });
    it('should return not found error if it cannot find unapproved item for that type', async () => {
      toObject = jest.fn().mockReturnValue(testItem);
      itemModel.findOne = jest.fn().mockResolvedValue(null);
      expect(itemModel.findOne).not.toHaveBeenCalled();
      const promise =  itemRepository.getSingleItemByType(ItemType.HEADLINES);
      await expect(promise).rejects.toThrow(NotFoundException);
      expect(itemModel.findOne).toHaveBeenCalledWith({ type: ItemType.HEADLINES, approved: { $exists: false }});
    });
  });

  describe('getApprovedItemsByType', () => {
    let toObject;
    it('should return approved items by provided type', async () => {
      toObject = jest.fn().mockReturnValue(testItem2);
      itemModel.find = jest.fn().mockResolvedValue( [{ toObject }]);
      expect(itemModel.find).not.toHaveBeenCalled();
      const result = await itemRepository.getApprovedItemsByType(ItemType.HEADLINES);
      expect(itemModel.find).toHaveBeenCalledWith({ type: ItemType.HEADLINES, approved: true});
      expect(result).toEqual([testItem2]);
    });
    it('should return not found error if it cannot find unapproved item for that type', async () => {
      toObject = jest.fn().mockReturnValue(testItem);
      itemModel.find = jest.fn().mockResolvedValue([]);
      expect(itemModel.find).not.toHaveBeenCalled();
      const result = await itemRepository.getApprovedItemsByType(ItemType.HEADLINES);
      expect(itemModel.find).toHaveBeenCalledWith({ type: ItemType.HEADLINES, approved: true});
      expect(result).toHaveLength(0);
    });
  });

  describe('setApprovalStatus', () => {
    let save;
    let toObject;
    const id = 'test_id';
    const status = true;
    const setApprovalStatusDto = {
      id,
      status
    };
    it('should return one unapproved item by provided type', async () => {
      save = jest.fn().mockImplementation(function () {
        return Promise.resolve(this)
      });
      toObject = jest.fn().mockImplementation(function () {
        return this;
      });
      itemModel.findOne = jest.fn().mockResolvedValue( { save, toObject });
      expect(itemModel.findOne).not.toHaveBeenCalled();
      const result = await itemRepository.setApprovalStatus(setApprovalStatusDto);
      expect(itemModel.findOne).toHaveBeenCalledWith({ id });
      expect(result.approved).toEqual(true);
    });
    it('should return not found error if it cannot find unapproved item for that type', async () => {
      itemModel.findOne = jest.fn().mockReturnValue(null);
      expect(itemModel.findOne).not.toHaveBeenCalled();
      const promise =  itemRepository.setApprovalStatus(setApprovalStatusDto);
      await expect(promise).rejects.toThrow(NotFoundException);
      expect(itemModel.findOne).toHaveBeenCalledWith({ id });
    });
  });

  describe('updateItem', () => {
    let toObject;
    const id = 'test_id';
    const source = 'test_source';
    const updateItemDto = {
      source
    };
    it('should return updated item', async () => {
      toObject = jest.fn().mockImplementation(function () {
        return this;
      });
      itemModel.findOneAndUpdate = jest.fn().mockResolvedValue( { source, toObject });
      expect(itemModel.findOneAndUpdate).not.toHaveBeenCalled();
      const result = await itemRepository.updateItem(id, updateItemDto);
      expect(itemModel.findOneAndUpdate).toHaveBeenCalledWith({ id }, updateItemDto, { new: true });
      expect(result.source).toEqual(source);
    });
    it('should return not found error if it cannot find item to update', async () => {
      itemModel.findOneAndUpdate = jest.fn().mockReturnValue(null);
      expect(itemModel.findOneAndUpdate).not.toHaveBeenCalled();
      const promise =  itemRepository.updateItem(id, updateItemDto);
      await expect(promise).rejects.toThrow(NotFoundException);
      expect(itemModel.findOneAndUpdate).toHaveBeenCalledWith({ id }, updateItemDto, { new: true});
    });
  });
});