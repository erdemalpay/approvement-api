export enum ItemType {
  HEADLINES = 'headlines',
};

export const ITEM_TYPES = [ItemType.HEADLINES];