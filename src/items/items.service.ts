import { compact } from 'lodash';
import { Injectable, Logger } from '@nestjs/common';
import { Item } from './item.schema';
import { CreateItemsDto } from './dto/create-items.dto';
import { ItemRepository } from './item.repository';
import { CreateItemDto } from './dto/create-item.dto';
import { ItemType } from './item-type.enum';
import { SetApprovalStatusDto } from './dto/set-approval-status.dto';
import { UpdateItemDto } from './dto/update-item.dto';

const logger = new Logger('bootstrap');

@Injectable()
export class ItemsService {
  constructor(
    private itemRepository: ItemRepository) {}

  async createItems(createItemsDto:CreateItemsDto): Promise<Array<Item>> {
    const { source, type, items } = createItemsDto;
    const savedItems = await Promise.all(items.map(itemData => {
        const createItemDto: CreateItemDto = {
          id: `${itemData.id}`,
          source,
          type,
          data: itemData
        }
        return this.itemRepository.create(createItemDto).catch((error) => {
          // We are just logging the items with existing id and ignoring it.
          // We may have updated it but since it is approved as it is, we should better not to update it.
          logger.warn(error.message);
        });
    }));
    return compact(savedItems);
  }

  async getSingleItemByType(type:ItemType): Promise<Item> {
    return this.itemRepository.getSingleItemByType(type);
  }
  
  async getApprovedItemsByType(type:ItemType): Promise<Item[]> {
    return this.itemRepository.getApprovedItemsByType(type);
  }

  async setApprovalStatus(setApprovalStatusDto:SetApprovalStatusDto): Promise<Item> {
    return this.itemRepository.setApprovalStatus(setApprovalStatusDto);
  }
  
  async updateItem(id: string, updateItemDto:UpdateItemDto): Promise<Item> {
    return this.itemRepository.updateItem(id, updateItemDto);
  }
}
