
import { ItemType } from './item-type.enum';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Item extends Document {

  @Prop({ required: true })
  id: string;

  @Prop()
  data: any;

  @Prop()
  type: ItemType;

  @Prop({ required: true })
  source: string;

  @Prop()
  approved?: boolean;
};

export const ItemSchema = SchemaFactory.createForClass(Item);
ItemSchema.index({ id: 1, source: 1 }, { unique: true });