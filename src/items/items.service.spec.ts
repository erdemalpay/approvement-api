import { Test, TestingModule } from '@nestjs/testing';
import { ItemsService } from './items.service';
import { MongooseModule } from '@nestjs/mongoose';
import { mongoUrl } from '../config/mongoose.config';
import { ItemRepository } from './item.repository';
import { ItemType } from './item-type.enum';
import { CreateItemsDto } from './dto/create-items.dto';
import { UpdateItemDto } from './dto/update-item.dto';

const mockItemRepository = () => jest.fn();

describe('ItemsService', () => {
  let itemService: ItemsService;
  let itemRepository: ItemRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [MongooseModule.forRoot(mongoUrl)],
      providers: [ItemsService, {
        provide: ItemRepository.name,
        useValue: mockItemRepository,
      }],
    }).compile();

    itemService = await module.get<ItemsService>(ItemsService);
    itemRepository = await module.get<ItemRepository>(ItemRepository);

  });

  it('should be defined', () => {
    expect(itemService).toBeDefined();
  });

  describe('createItems', () => {
    let createItemsDto: CreateItemsDto;
    const testItem1 = {
      id: 'item1'
    };
    const testItem2 = {
      id: 'item2'
    };
    const testItem3 = {
      id: 'item3'
    };
    const testItem2_2 = {
      id: 'item2'
    };
    beforeEach(() => {
      const itemsList = [];
      itemRepository.create = jest.fn().mockImplementation(item => {
        if (itemsList.includes(item.id)) {
          return Promise.reject({ code: 11000 });
        }
        itemsList.push(item.id);
        return Promise.resolve(item);
      });
    });

    it('should save all items in items field', async () => {
        createItemsDto = {
          source: 'testSource',
          type: ItemType.HEADLINES,
          items: [testItem1, testItem2, testItem3]
        };
        const result = await itemService.createItems(createItemsDto);
        expect(itemRepository.create).toHaveBeenCalledTimes(3);
        expect(result).toHaveLength(3);
    });

    it('should only add elements with unique id', async () => {
      createItemsDto = {
        source: 'testSource',
        type: ItemType.HEADLINES,
        items: [testItem1, testItem2, testItem3, testItem2_2]
      };
      const result = await itemService.createItems(createItemsDto);
      expect(itemRepository.create).toHaveBeenCalledTimes(4);
      expect(result).toHaveLength(3);
    });
  });

  describe('getItemByType', () => {
    const testItem = { dummy: 'item' };
    beforeEach(() => {
      itemRepository.getSingleItemByType = jest.fn().mockReturnValue(testItem);
    });
    it('should call getSingleItemByType method of repository', async () => {
      expect(itemRepository.getSingleItemByType).not.toHaveBeenCalled();
      const result = await itemService.getSingleItemByType(ItemType.HEADLINES);
      expect(itemRepository.getSingleItemByType).toHaveBeenCalledWith(ItemType.HEADLINES);
      expect(result).toEqual(testItem);
    });
  });
 
  describe('getApprovedItemsByType', () => {
    const testItem = { dummy: 'item' };
    const testItem2 = { dummy: 'item2' };
    const testItems = [testItem, testItem2];
    beforeEach(() => {
      itemRepository.getApprovedItemsByType = jest.fn().mockReturnValue(testItems);
    });
    it('should call getApprovedItemsByType method of repository', async () => {
      expect(itemRepository.getApprovedItemsByType).not.toHaveBeenCalled();
      const result = await itemService.getApprovedItemsByType(ItemType.HEADLINES);
      expect(itemRepository.getApprovedItemsByType).toHaveBeenCalledWith(ItemType.HEADLINES);
      expect(result).toEqual(testItems);
    });
  });
  
  describe('setApprovalStatus', () => {
    const testItem = { dummy: 'item' };
    const id = 'test_id';
    const status = true;
    const setApprovalStatusDto = {
      id,
      status
    };
    beforeEach(() => {
      itemRepository.setApprovalStatus = jest.fn().mockReturnValue(testItem);
    });
    it('should call setApprovalStatus method of repository', async () => {
      expect(itemRepository.setApprovalStatus).not.toHaveBeenCalled();
      const result = await itemService.setApprovalStatus(setApprovalStatusDto);
      expect(itemRepository.setApprovalStatus).toHaveBeenCalledWith(setApprovalStatusDto);
      expect(result).toEqual(testItem);
    });
  });

  describe('updateItem', () => {
    const testItem = { dummy: 'item' };
    const id = 'test_id';
    const source = 'test_source';
    const updateItemDto:UpdateItemDto = {
      source
    };
    beforeEach(() => {
      itemRepository.updateItem = jest.fn().mockReturnValue(testItem);
    });
    it('should call updateItem method of repository', async () => {
      expect(itemRepository.updateItem).not.toHaveBeenCalled();
      const result = await itemService.updateItem(id, updateItemDto);
      expect(itemRepository.updateItem).toHaveBeenCalledWith(id, updateItemDto);
      expect(result).toEqual(testItem);
    });
  });
});
