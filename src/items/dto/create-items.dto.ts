import { Item } from "../item.schema";
import { ItemType, ITEM_TYPES } from "../item-type.enum";
import { IsNotEmpty, IsIn, IsArray } from "class-validator";

export class CreateItemsDto {
  @IsArray()
  items: Array<Partial<Item>>;
  
  @IsNotEmpty()
  source: string;
  
  @IsIn(ITEM_TYPES)
  type: ItemType;
}