import { ItemType, ITEM_TYPES } from "../item-type.enum";
import { IsNotEmpty, IsIn } from "class-validator";

export class CreateItemDto {
  @IsNotEmpty()
  id: string;
  
  @IsNotEmpty()
  data: any;

  @IsNotEmpty()
  source: string;
  
  @IsIn(ITEM_TYPES)
  type: ItemType;

  approved?: boolean
}