import { IsNotEmpty } from "class-validator";

export class SetApprovalStatusDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  status: boolean;
}