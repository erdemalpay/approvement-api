import { Controller, Post, Logger, Get, Query, Body, Put, Param, ValidationPipe, UseGuards } from '@nestjs/common';
import { CreateItemsDto } from './dto/create-items.dto';
import { Item } from './item.schema';
import { ItemsService } from './items.service';
import { ItemType } from './item-type.enum';
import { SetApprovalStatusDto } from './dto/set-approval-status.dto';
import { UpdateItemDto } from './dto/update-item.dto';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard())
@Controller('items')
export class ItemsController {
  private logger = new Logger('ItemsController');
  constructor(private itemsService: ItemsService) {}

  @Post()
  createItems(
    @Body(ValidationPipe)
    createItemsDto: CreateItemsDto
  ): Promise<Item[]> {
    return this.itemsService.createItems(createItemsDto);
  }

  @Get()
  getSingleItemByType(
    @Query('type')
    type: ItemType
  ): Promise<Item>{
    return this.itemsService.getSingleItemByType(type);
  }
  
  @Get('approved')
  getApprovedItemsByType(
    @Query('type')
    type: ItemType
  ): Promise<Item[]>{
    return this.itemsService.getApprovedItemsByType(type);
  }

  @Put('status/:id')
  setApprovalStatus(
    @Param('id')
    id: string,
    @Body('status')
    status: boolean,
  ): Promise<Item>{
    const setApprovalStatusDto:SetApprovalStatusDto = {
      id,
      status
    };
    return this.itemsService.setApprovalStatus(setApprovalStatusDto);
  }
  
  @Put(':id')
  updateItem(
    @Param('id')
    id: string,
    @Body()
    updateItemDto: UpdateItemDto,
  ): Promise<Item>{
    return this.itemsService.updateItem(id, updateItemDto);
  }
}
