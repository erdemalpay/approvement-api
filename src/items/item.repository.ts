import { Injectable, ConflictException, NotFoundException } from "@nestjs/common";
import { Model } from "mongoose";
import { Item } from './item.schema';
import { InjectModel } from "@nestjs/mongoose";
import { CreateItemDto } from "./dto/create-item.dto";
import { ItemType } from "./item-type.enum";
import { SetApprovalStatusDto } from "./dto/set-approval-status.dto";
import { UpdateItemDto } from "./dto/update-item.dto";

async function purify(item:Item): Promise<Item> {
  const tempItem = item.toObject();
  delete tempItem._id;
  delete tempItem.__v;
  return tempItem;
}

@Injectable()
export class ItemRepository {
  constructor(
    @InjectModel(Item.name)
    private itemModel: Model<Item>) {}

  async create(itemDto: CreateItemDto): Promise<Item> {
    try {
      const item = await this.itemModel.create(itemDto);
      return purify(item);
    } catch (error) {
      if (error.code === 11000) { // duplicate item
        throw(new ConflictException(`Item with id ${itemDto.id} already exists`));
      }
      throw(error);
    }
  }

  async getSingleItemByType(type: ItemType): Promise<Item> {
    const item = await this.itemModel.findOne({ type, approved: {$exists: false } });
    if (!item) {
      throw (new NotFoundException(`Item with type ${type} does not exist`));
    }
    return purify(item);
  }
  
  async getApprovedItemsByType(type: ItemType): Promise<Item[]> {
    const items = await this.itemModel.find({ type, approved: true });
    return Promise.all(items.map(purify));
  }

  async setApprovalStatus(setApprovalStatusDto:SetApprovalStatusDto): Promise<Item> {
    const { id, status } = setApprovalStatusDto;
    let item = await this.itemModel.findOne({ id });
    if (!item) {
      throw (new NotFoundException(`Item with id ${id} does not exist`));
    }
    item.approved = status;
    item = await item.save();
    return purify(item);
  }

  async updateItem(id: string, updateItemDto: UpdateItemDto): Promise<Item> {
    const item = await this.itemModel.findOneAndUpdate({ id }, updateItemDto, { new: true });
    if (!item) {
      throw (new NotFoundException(`Item with id ${id} does not exist`));
    }
    return purify(item);
  }
}