import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import { ItemType } from '../items/item-type.enum';

@Schema()
export class User extends Document {
  @Prop({ unique: true, required: true })
  username: string;

  @Prop({ required: true })
  password: string;

  @Prop()
  salt?: string;
  
  @Prop()
  admin?: boolean = false;

  @Prop()
  access?: Array<ItemType> = [];
}

export const UserSchema = SchemaFactory.createForClass(User);
UserSchema.index({ username: 1 }, { unique: true });

UserSchema.methods.toJSON = function() {
  const obj = this.toObject();
  delete obj.password;
  delete obj.salt;
  delete obj._id;
  delete obj.__v;
  return obj;
 }
 