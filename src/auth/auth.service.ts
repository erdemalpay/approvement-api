import { Injectable, UnauthorizedException, Inject } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.schema';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class AuthService {
  constructor(
    @Inject(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async signup(authCredentialsDto: AuthCredentialsDto) : Promise<void> {
    const createUserDto: CreateUserDto = {
      ...authCredentialsDto,
      admin: false,
      access: []
    };
    return this.userRepository.createUser(createUserDto);
  }

  async signIn(authCredentialsDto: AuthCredentialsDto) : Promise<{ accessToken: string}> {
    const username = await this.userRepository.validateUserPassword(authCredentialsDto);
    if (!username) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const payload: JwtPayload = { username };
    const accessToken = await this.jwtService.sign(payload);

    return { accessToken };
  }

  async createUser(createUserDto: CreateUserDto) : Promise<void> {
    return this.userRepository.createUser(createUserDto);
  }
  
  async updateUser(updateUserDto: UpdateUserDto) : Promise<User> {
    return this.userRepository.updateUser(updateUserDto);
  }
  
  async getUsers() : Promise<Array<User>> {
    return this.userRepository.getAll();
  }
}
