import { UserRepository } from "./user.repository";
import { Test } from "@nestjs/testing";
import { User } from "./user.schema";
import * as bcrypt from 'bcryptjs';
import { ConflictException } from "@nestjs/common";
import { getModelToken } from "@nestjs/mongoose";

const mockCredentialsDto = { username: 'Test', password: 'Test password'};
const mockCreateUserDto = { username: 'Test', password: 'Test password', admin: false, access: []};
const mockUserModel = () => ({});

describe('User Repository', () => {
  let userRepository;
  let userModel;

  beforeEach(async () => {
    const userModelToken = getModelToken(User.name);
    const module = await Test.createTestingModule({
      providers: [
        UserRepository, {
          provide: userModelToken,
          useFactory: mockUserModel,
        }
      ]
    }).compile();

    userRepository = await module.get<UserRepository>(UserRepository);
    userModel = await module.get<User>(userModelToken);
  })
  describe('createUser', () => {
    let save;

    beforeEach(() => {
      save = jest.fn();
      userModel.create = jest.fn().mockReturnValue( { save });
    });

    it('successfully signs up the user', async () => {
      save.mockResolvedValue(undefined);
      expect(userRepository.createUser(mockCreateUserDto)).resolves.not.toThrow();
    });
    it('throws a conflict exception as user already exists', async () => {
      save.mockRejectedValue({ code: 11000 });
      await expect(userRepository.createUser(mockCreateUserDto)).rejects.toThrow(ConflictException);
    });
    it('throws the error if any other error occurs other than already exists ', async () => {
      save.mockRejectedValue(new Error('12345'));
      await expect(userRepository.createUser(mockCreateUserDto)).rejects.toThrow(Error);
    });
  });

  describe('validateUserPassword', () => {
    let user;

    beforeEach(() => {
      user = {
        username: 'TestUsername',
        salt: 'randomsalt',
        password: 'HashedPassword'
      };
    });

  it('returns the username as validation is successful', async () => {
      userModel.findOne = jest.fn().mockReturnValue(user);
      bcrypt.hash = jest.fn().mockResolvedValue('HashedPassword');
      const username = await userRepository.validateUserPassword(mockCredentialsDto);
      expect(username).toEqual('TestUsername');
    });
    
  it('returns null as user is not found', async () => {
      userModel.findOne = jest.fn().mockReturnValue(null);
      const username = await userRepository.validateUserPassword(mockCredentialsDto);
      expect(username).toBeNull();
    });
    
  it('returns null as password is not valid', async () => {
      userModel.findOne = jest.fn().mockReturnValue(user);
      bcrypt.hash = jest.fn().mockResolvedValue('WrongPassword');
      const username = await userRepository.validateUserPassword(mockCredentialsDto);
      expect(username).toBeNull();
    });
  });

  describe('hashPassword', () => {
    it('should call bcyrptjs.hashPassword', async () => {
      bcrypt.hash = jest.fn().mockResolvedValue('HashedPassword');
      expect(bcrypt.hash).not.toHaveBeenCalled();
      const hashedPassword = await userRepository.hashPassword('testPassword', 'testSalt');
      expect(hashedPassword).toEqual('HashedPassword');
      expect(bcrypt.hash).toHaveBeenCalledWith('testPassword', 'testSalt');
    });
  });

  describe('findOne', () => {
    it('should find and return a user according to input parameters', async() => {
      const dummyUser = {};
      userModel.findOne = jest.fn().mockReturnValue(dummyUser);
      expect(userModel.findOne).not.toHaveBeenCalled();
      const resultUser = await userRepository.findOne(dummyUser);
      expect(resultUser).toEqual(dummyUser);
      expect(userModel.findOne).toHaveBeenCalledWith(dummyUser);
    });
  });
});