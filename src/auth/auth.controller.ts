import { Controller, Get, Post, Body, ValidationPipe, UseGuards, Put } from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody } from '@nestjs/swagger';
import { GetUser } from './get-user.decorator';
import { User } from './user.schema';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller('auth')
export class AuthController {

  constructor(
    private authService: AuthService,
  ) {}

  @Post('/signup')
  async signup(
    @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto
  ) : Promise<void>{
    return this.authService.signup(authCredentialsDto);
  }

  @Post('/signin')
  async signIn(
    @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto
  ) : Promise<{accessToken: string}> {
    return this.authService.signIn(authCredentialsDto);
  }
  
  @UseGuards(AuthGuard())
  @Get('/user')
  async getProfile(
    @GetUser() user:User
  ) : Promise<Partial<User>> {
    return user;
  }

  @UseGuards(AuthGuard())
  @Get('/users')
  async getUsers(
  ) : Promise<Array<Partial<User>>> {
    const users = await this.authService.getUsers();
    return users;
  }

  @UseGuards(AuthGuard())
  @Post('/create')
  @ApiBody({ type: [CreateUserDto] })
  async createUser(
    @Body(ValidationPipe) createUserDto: CreateUserDto
  ) : Promise<void>{
    return this.authService.createUser(createUserDto);
  }
  
  @UseGuards(AuthGuard())
  @Put('/user/update')
  async updateUser(
    @Body(ValidationPipe) updateUserDto: UpdateUserDto
  ) : Promise<User>{
    return this.authService.updateUser(updateUserDto);
  }
}
