import { JwtStrategy } from './jwt.strategy';
import { Test } from '@nestjs/testing';
import { UserRepository } from './user.repository';
import { User, UserSchema } from './user.schema';
import { UnauthorizedException } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';

const mockUserRepository = () => ({
  findOne: jest.fn()
});

describe('JWT Strategy', () => {
  let jwtStrategy: JwtStrategy;
  let userRepository;

  beforeEach(async () => {
    try {
    const module = await Test.createTestingModule({
      providers: [
        JwtStrategy,
        {
          provide: UserRepository,
          useFactory: mockUserRepository
        },
        {
          provide: getModelToken(User.name),
          useValue: UserSchema,
        }
      ]
    }).compile();
    jwtStrategy = await module.get<JwtStrategy>(JwtStrategy);
    userRepository = await module.get<UserRepository>(UserRepository);
  } catch(e) {
    console.error(e);
  }
  });

  describe('validate', () => {
    it('should return validated user if found', async () => {
        const user = {
          username: 'TestUser',
        };
        userRepository.findOne.mockReturnValue(user);
        const result = await jwtStrategy.validate({ username: 'TestUsername'});
        expect(userRepository.findOne).toHaveBeenCalledWith({ username: 'TestUsername' });
        expect(result).toEqual(user);
    });

    it('should throw Unauthorized exception if user not found', async () => {
        userRepository.findOne.mockReturnValue(null);
        expect(jwtStrategy.validate({ username: 'TestUsername'})).rejects.toThrow(UnauthorizedException);
        expect(userRepository.findOne).toHaveBeenCalledWith({ username: 'TestUsername' });
    });
  });
});