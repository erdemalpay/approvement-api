import { IsString, MinLength, MaxLength, Matches, IsArray } from "class-validator";
import { ItemType } from "src/items/item-type.enum";
import { ApiProperty } from '@nestjs/swagger';


export class CreateUserDto {
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  @ApiProperty()
  username: string;

  @IsString()
  @MinLength(4)
  @MaxLength(20)
  password: string;

  admin? = false;

  @IsArray()
  access?: Array<ItemType> = [];
}