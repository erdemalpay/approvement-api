import { IsString, IsArray } from "class-validator";
import { ItemType } from "src/items/item-type.enum";

export class UpdateUserDto {
  @IsString()
  username: string;

  admin: boolean;

  @IsArray()
  access: Array<ItemType> = [];
}